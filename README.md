=== WP bbPress Export ===

Requires PHP: 5.6

License: GPLv2 or later

License URI: https://www.gnu.org/licenses/gpl-2.0.html

Helps in exporting one or more bbPress forums along with forum posts and replies with username and timestamp into a word doc file.

Please make sure that bbPress is installed and activated before using this plugin or else a warning will be shown requesting to install and acitvate bbPress plugin.

This plugin uses PHPWORD library for writing in the word file.

Github link for PHPWORD: https://github.com/PHPOffice/PHPWord

Please note that PHPWORD is already included in this plugin.

PHPWORD Documentation: https://phpword.readthedocs.io/en/latest/
