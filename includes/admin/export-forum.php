<?php
function bbpress_forum_export_handle_post(){

 require_once 'library/phpword/bootstrap.php';

    $phpWord = new \PhpOffice\PhpWord\PhpWord();
    $section = $phpWord->addSection();

    $sforum = $_POST['forum'];
    foreach($sforum as $sf)
    {
        $forumT = get_the_title($sf);
        $phpWord->addParagraphStyle('p1Style', array('align'=>'center', 'spaceAfter'=>100));
        $ftitle = $section->addTextRun('p1Style');
        $ftitle->addText(htmlspecialchars(  'Forum Title: '.$forumT  ), array('size' => 18));
        $args = array(
            'post_parent' => $sf,
            'posts_per_page' => -1,
            'post_type'   => 'topic'
        );

        $topics = new WP_Query( $args );
        while ( $topics->have_posts() ) : $topics->the_post();
            $section->addText( 'Topic Title: '.get_the_title() );
            $section->addText( 'Topic Content: '.get_the_content() );
            $phpWord->addParagraphStyle('p2Style', array('align'=>'right', 'spaceAfter'=>100));
            $textrun = $section->addTextRun('p2Style');
            $author_id = get_the_author_meta( 'ID' );
            $textrun->addText(htmlspecialchars('Posted by: '.get_the_author_meta( 'nicename', $author_id ).' Posted on: '.get_the_date('Y-m-d H:i:s')));
            $section->addText( 'Replies: ' );
            $rargs = array(
                'post_parent' => get_the_ID(),
                'posts_per_page' => -1,
                'post_type'   => 'reply'
            );
            $replies = new WP_Query( $rargs );
            while ( $replies->have_posts() ) : $replies->the_post();
                $section->addText( get_the_content());
                $textrun1 = $section->addTextRun('p2Style');
                $reply_author_id = get_the_author_meta( 'ID' );
                $textrun1->addText(htmlspecialchars('Posted by: '.get_the_author_meta( 'nicename', $reply_author_id ).' Posted on: '.get_the_date('Y-m-d H:i:s')));
            endwhile;
        endwhile;
    }

    $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save(dirname(__FILE__).'/doc/Forum.docx');
    echo json_encode(array( "status" => "success", "fpath" => dirname(__FILE__)."/doc/Forum.docx", "dfile"=>plugin_dir_url( __FILE__ ).'save.php' ));

    die();
}

add_action('wp_ajax_bbpress_forum_export', 'bbpress_forum_export_handle_post');
?>