<?php

/**
 * Add the top level menu page.
 */
function wpb_bbpress_export_options_page() {
    add_menu_page(
        'Forum Export',
        'Forum Export',
        'manage_options',
        'wpb-bbpress-export',
        'wpb_bbpress_export_page_html'
    );
}


/**
 * Register our wpb_options_page to the admin_menu action hook.
 */
add_action( 'admin_menu', 'wpb_bbpress_export_options_page' );

/**
 * Top level menu callback function
 */
function wpb_bbpress_export_page_html() {
    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
        return;
    }

    // add error/update messages

    // check if the user have submitted the settings
    // WordPress will add the "settings-updated" $_GET parameter to the url
    if ( isset( $_GET['settings-updated'] ) ) {
        // add settings saved message with the class of "updated"
        add_settings_error( 'wpb_messages', 'wpb_message', __( 'Settings Saved', 'wpb' ), 'updated' );
    }

    // show error/update messages
    settings_errors( 'wpb_messages' );

    if ( !class_exists( 'bbPress' ) ) {
        echo 'Please make sure bbpress plugin is installed and activated to continue.';
        return;
    }
    ?>
    <div class="wrap">
        <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

            <?php
             $args = array(
                'posts_per_page' => -1,
                'post_type'   => 'forum'
              );
            $my_query = new WP_Query( $args );

            if ( $my_query->have_posts() ) :
            ?>
                <label>Select Forum to export:</label>
                <select id="forum" name="forum" class="selectpicker" multiple>
            <?php
                while ( $my_query->have_posts() ) : $my_query->the_post();
            ?>

            <?php
                    echo '<option value="'.get_the_ID().'">'.get_the_title().'</option>';
                endwhile;
            ?>
                </select>
            <?php

            else :
                echo 'No Forums added yet';
                return;
            endif;

            ?>


            <?php
            submit_button( 'Export' );
            ?>
    </div>
    <?php


}

function wp_bbpress_export_load_scripts()
{
    if($_GET['page'] == 'wpb-bbpress-export')
    {
        wp_enqueue_style( 'bootstrap_css', plugins_url('css/bootstrap.min.css',__FILE__ ) );
        wp_enqueue_style( 'select_css', plugins_url('css/bootstrap-select.min.css',__FILE__ ) );
        wp_enqueue_script('popper_js', plugins_url('js/popper.min.js',__FILE__ ));
        wp_enqueue_script('bootstrap_js', plugins_url('js/bootstrap.min.js',__FILE__ ));
        wp_enqueue_script('select_js', plugins_url('js/bootstrap-select.min.js',__FILE__ ));
        wp_enqueue_script('boot_js', plugins_url('js/export-ajax.js',__FILE__ ));
    }

}
add_action('admin_enqueue_scripts', 'wp_bbpress_export_load_scripts');


?>