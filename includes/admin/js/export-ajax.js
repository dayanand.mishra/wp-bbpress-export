jQuery(document).ready(function ($) {
    $("#submit").click(function () {
        var f = $("#forum").val();
        if (f.length == 0) {
            alert("Please select atleast one forum to continue");
        } else if (f.length == 1 && f[0] == "select") {
            alert("Please select atleast one forum to continue");
        } else {
            var data = {
                action: "bbpress_forum_export",
                forum: $("#forum").val(),
            };
            $.post(ajaxurl, data, function (response) {
                var data = JSON.parse(response);
                if (data.status == "success") {
                    $(".selectpicker").selectpicker("deselectAll");
                    window.location.href = data.dfile;
                }
            });
        }
    });
});
