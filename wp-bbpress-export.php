<?php
/**
 * Plugin Name: WP bbPress Export
 * Plugin URI: https://pippinsplugins.com/how-to-begin-writing-your-first-wordpress-plugin
 * Description: This plugin helps in exporting the bbPress forums along with topics and replies into a word doc file.
 * Author: Dayanand Mishra
 * Author URI: https://two.wordpress.com
 * Version: 1.0
 */

include('includes/includes.php');
 ?>